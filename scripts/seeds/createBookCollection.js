const logger = require('@pubsweet/logger')
const {
  model: BookCollection,
} = require('editoria-data-model/src/bookCollection')
const {
  model: BookCollectionTranslation,
} = require('editoria-data-model/src/bookCollectionTranslation')

const createBookCollection = async () => {
  let collection

  await new BookCollection().save().then(res => (collection = res))

  logger.info(collection)

  await new BookCollectionTranslation({
    collectionId: collection.id,
    languageIso: 'en',
    title: 'Books',
  })
    .save()
    .then(res => logger.info(res))
}

module.exports = createBookCollection
